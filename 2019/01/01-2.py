rows = []
with open("input.txt") as f:
	rows = [int(x.strip()) for x in f.readlines()]

def calc_fuel(mass):
	fuel = (mass // 3) - 2
	if fuel > 0:
		fuel += calc_fuel(fuel)
	return max(0, fuel)

fuel = 0
for mass in rows:
	fuel += calc_fuel(mass)

print(fuel)