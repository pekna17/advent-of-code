rows = []
with open("input.txt") as f:
	rows = [((int(x.strip())//3)-2) for x in f.readlines()]
print(sum(rows))