rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]

index = 0
group_answers1 = [[]]
group_answers2 = [[]]
for row in rows:
	if row == "":
		index += 1
		group_answers1.append([])
		group_answers2.append([])
		continue

	# Join answers for part 1
	group_answers1[index] = set(group_answers1[index]) | set(row)
	# Keep individual answers for part 2
	group_answers2[index].append(set(row))

count1 = 0
for answer in group_answers1:
	count1 += len(answer)
print(count1)

count2 = 0
for answer in group_answers2:
	# Count what is common per group
	count2 += len(set.intersection(*answer))
print(count2)