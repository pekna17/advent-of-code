import re

rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]

index = 0
passports = [{}]
for row in rows:
	if row == "":
		index += 1
		passports.append({})
		continue

	parts = row.split(" ")
	for part in parts:
		key, value = part.split(":")
		passports[index][key] = value

counter = 0
counter2 = 0
for passport in passports:
	if len(passport) == 8 or (len(passport) == 7 and "cid" not in passport):
		counter += 1
		if (1920 <= int(passport["byr"]) <= 2002
				and 2010 <= int(passport["iyr"]) <= 2020
				and 2020 <= int(passport["eyr"]) <= 2030
				and ((passport["hgt"].endswith("cm") and (150 <= int(passport["hgt"][:-2]) <= 193))
					or (passport["hgt"].endswith("in") and (59 <= int(passport["hgt"][:-2]) <= 76)))
				and re.match(r"^#[\da-f]{6}$", passport["hcl"])
				and passport["ecl"] in "amb blu brn gry grn hzl oth".split(" ")
				and re.match(r"^[\d]{9}$", passport["pid"])):
			counter2 += 1
print(counter)
print(counter2)
