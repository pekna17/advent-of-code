rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]

counter = 0
index = 0
for row in rows:
	if row[index % len(row)] == "#":
		counter += 1
	index += 3
print(counter)
