rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]


def calc(rows, stepx, stepy=1):
	counter = 0
	index = 0
	for row in range(0, len(rows), stepy):
		if rows[row][index % len(rows[row])] == "#":
			counter += 1
		index += stepx
	return counter


print(calc(rows, 1) * calc(rows, 3) * calc(rows, 5) * calc(rows, 7) * calc(rows, 1, 2))
