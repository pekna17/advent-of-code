import re

rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]

bags = {}
for row in rows:
	bag, part2 = row.split(" bags contain ")
	bags[bag] = []
	contains = re.findall(r"(\d+) (\w+ \w+) bag", part2)
	for cont in contains:
		bags[bag].append(cont)


def find_bag(bags, bag, name):
	for sub_bag in bag:
		if sub_bag[1] == name:
			return True
		else:
			if find_bag(bags, bags[sub_bag[1]], name):
				return True
	return False


lista = {}
for name, bag in bags.items():
	if find_bag(bags, bag, "shiny gold"):
		lista[name] = bag
print(len(lista))


# Part 2
def count_bags(bags, bag):
	counter = 1
	for sub_bag in bag:
		counter += count_bags(bags, bags[sub_bag[1]]) * int(sub_bag[0])
	return counter


counter = count_bags(bags, bags["shiny gold"])
# Remove one for it self
print(counter-1)