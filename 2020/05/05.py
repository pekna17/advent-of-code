rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]


def get_position(data, size, lower_key, higher_key):
	positions = []
	positions.extend(range(0, size))
	for index in range(0, len(data)):
		pos = int(len(positions)/2)
		if data[index] == lower_key:
			positions = positions[:-pos]
		if data[index] == higher_key:
			positions = positions[pos:]
	return positions[0]
	

# Part 1
seats = []
for row in rows:
	seat_row = get_position(row[0:7], 128, "F", "B")
	seat_seat = get_position(row[7:10], 8, "L", "R")
	seat_id = seat_row * 8 + seat_seat
	seats.append(seat_id)
print("Part1: %s" % max(seats))

# Part 2
seats = sorted(seats)
seat_before = [x for index, x in enumerate(seats) if index < len(seats)-1 and seats[index+1] - seats[index] > 1]
print("Part2: %s" % (seat_before[0] + 1))
