rows = []
with open("input.txt") as f:
	rows = [int(x.strip()) for x in f.readlines()]

goal = 2020
for index1, num1 in enumerate(rows):
	for index2, num2 in enumerate(rows[index1:]):
		for index3, num3 in enumerate(rows[index2:]):
			res = num1 + num2 + num3
			if goal == res:
				print("%s+%s+%s=%s" % (num1, num2, num3, res))
				print("Answer: %s" % (num1 * num2 * num3))
				exit(0)
