rows = []
with open("input.txt") as f:
	rows = [int(x.strip()) for x in f.readlines()]

goal = 2020
for num in rows:
	diff = goal - num
	if diff in rows:
		print("%s+%s=%s" % (num, diff, goal))
		print("Answer: %s" % (diff * num))
		exit(0)
