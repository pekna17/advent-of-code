import re

rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]

counter = 0
regexp = re.compile("(\d+)-(\d+) ([a-z]): ([a-z]*)")
for index, row in enumerate(rows):
	first_pos, second_pos, letter, password = regexp.findall(row)[0]
	first_pos = int(first_pos) - 1
	second_pos = int(second_pos) - 1
	if (letter == password[first_pos]) is not (letter == password[second_pos]):
		counter += 1
print(counter)
