import re

rows = []
with open("input.txt") as f:
	rows = [x.strip() for x in f.readlines()]

counter = 0
regexp = re.compile("(\d+)-(\d+) ([a-z]): ([a-z]*)")
for index, row in enumerate(rows):
	lower_limit, upper_limit, letter, password = regexp.findall(row)[0]
	if int(lower_limit) <= password.count(letter) <= int(upper_limit):
		counter += 1
print(counter)
