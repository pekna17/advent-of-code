import re

rows = []
with open("input.txt") as f:
	ops = [{"op": x.strip().split(" ")[0], "step": int(x.strip().split(" ")[1]), "count": 0} for x in f.readlines()]


def test(ops):
	# First reset counter
	ops = [{"op": x["op"], "step": x["step"], "count": 0} for x in ops]
	index = 0
	accumulator = 0
	while ops[index]["count"] == 0:
		ops[index]["count"] += 1
		if ops[index]["op"] == "acc":
			accumulator += ops[index]["step"]
			index += 1
		elif ops[index]["op"] == "jmp":
			index += ops[index]["step"]
		else:
			index += 1
		if index >= len(ops):
			return "Success", accumulator
	return "Loop detected", accumulator


_, accumulator = test(ops)
print(accumulator)


# part 2 - brute force
def swapit(op):
	if op["op"] == "jmp":
		op["op"] = "nop"
	elif op["op"] == "nop":
		op["op"] = "jmp"
	return op


for index, op in enumerate(ops):
	ops[index] = swapit(op)
	result, accumulator = test(ops)
	if result == "Success":
		print(accumulator)
		break
	ops[index] = swapit(ops[index])
