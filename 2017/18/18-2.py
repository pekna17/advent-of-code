from collections import defaultdict
steps = []
with open("18-1.txt") as f:
	steps = [x.strip().split() for x in f.readlines()]

program = defaultdict(int)

def get(value, program):
	if value in program:
		return program[value]
	else:
		return int(value)

def move(program, index, rcv_step, snd_step, counter):
	while index >= 0 and index < len(steps):
		step = steps[index]
		if step[0] == "snd":
			snd_step.append(get(step[1], program))
			if counter is not None:
				counter += 1
		if step[0] == "set":
			program[step[1]] = get(step[2], program)
		if step[0] == "add":
			program[step[1]] += get(step[2], program)
		if step[0] == "mul":
			program[step[1]] *= get(step[2], program)
		if step[0] == "mod":
			program[step[1]] %= get(step[2], program)
		if step[0] == "rcv":
			if len(rcv_step):
				program[step[1]] = rcv_step.pop(0)
			else:
				return step[0], index, counter
		skip = 1
		if step[0] == "jgz":
			if (get(step[1], program) > 0):
				skip = get(step[2], program)
		index = (index + skip) % len(steps)

prog_a = program.copy()
prog_b = program.copy()
prog_b['p'] = 1

index_a = 0
index_b = 0

state_a = ""
state_b = ""

a_to_b = []
b_to_a = []

counter = 0

while True:
	state_a, index_a, _ = move(prog_a, index_a, b_to_a, a_to_b, None)
	state_b, index_b, counter = move(prog_b, index_b, a_to_b, b_to_a, counter)
	if state_a == "rcv" and state_b == "rcv" and len(b_to_a) == 0 and len(a_to_b) == 0:
		break
print "%s send from program 1" % counter
