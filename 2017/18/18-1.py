from collections import defaultdict
steps = []
with open("18-1.txt") as f:
	steps = [x.strip().split() for x in f.readlines()]

program = defaultdict(int)

def get(value, program):
	if value in program:
		return program[value]
	else:
		return int(value)

def move(program, reps):
	last_sound = -1
	index = 0
	while index >= 0 and index < len(steps):
		step = steps[index]
		if step[0] == "snd":
			last_sound = get(step[1], program)
		if step[0] == "set":
			program[step[1]] = get(step[2], program)
		if step[0] == "add":
			program[step[1]] += get(step[2], program)
		if step[0] == "mul":
			program[step[1]] *= get(step[2], program)
		if step[0] == "mod":
			program[step[1]] %= get(step[2], program)
		if step[0] == "rcv":
			if (get(step[1], program) > 0):
				print "Replay sound: %s" % last_sound
				break
		skip = 1
		if step[0] == "jgz":
			if (get(step[1], program) > 0):
				skip = get(step[2], program)
		index += skip
	return program

move(program.copy(), 1)
