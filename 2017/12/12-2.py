rows = []
with open("12-1.txt") as f:
	rows = [x.strip() for x in f.readlines()]

data = {}
for row in rows:
	index, connections = row.split(" <-> ")
	connections = connections.split(", ")
	if index in data:
		for conn in connections:
			if conn not in data[index]:
				data[index] += conn
	else:
		data[index] = connections[:]

def get_unique(data, number, visited):
	if number in visited:
		return False
	visited += [number]
	for num in data[number]:
		get_unique(data, num, visited)
	return True

count = 0
visited = []
for item in data:
	if get_unique(data, item, visited):
		count += 1
print "Found %s" % count