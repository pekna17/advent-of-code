rows = []
with open("12-1.txt") as f:
	rows = [x.strip() for x in f.readlines()]

data = {}
for row in rows:
	index, connections = row.split(" <-> ")
	connections = connections.split(", ")
	if index in data:
		for conn in connections:
			if conn not in data[index]:
				data[index] += conn
	else:
		data[index] = connections[:]

def find_zero(data, number, visited):
	if number in visited:
		return False
	if number == '0':
		return True
	visited += [number]
	for num in data[number]:
		if find_zero(data, num, visited[:]):
			return True
	return False

count = 0
for item in data:
	if find_zero(data, item, []):
		count += 1
print "Found %s" % count