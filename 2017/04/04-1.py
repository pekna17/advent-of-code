rows = []
with open("04-1.txt") as f:
	rows = [x.strip().split() for x in f.readlines()]

valid_passphrases = 0
for row in rows:
	if (len(row) == len(set(row))):
		valid_passphrases += 1

print "%s of %s valid passphrases" % (valid_passphrases, len(rows))