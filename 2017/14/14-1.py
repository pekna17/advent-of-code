data = "flqrgnkx" # Test
data = "xlqgujun"

def calc_hash(data):
	the_list = list(range(256))
	steps = [ord(x) for x in list(data)] + [17, 31, 73, 47, 23]
	skip_size = 0
	position = 0
	for the_round in range(64):
		for step in steps:
			list_to_reverse = []
			for x in range(step):
				index = (position + x) % 256
				list_to_reverse.append(the_list[index])
			list_to_reverse.reverse()
			
			for x in range(step):
				index = (position + x) % 256
				the_list[index] = list_to_reverse[x]

			position = (position + step + skip_size) % 256
			skip_size += 1

	knot_hash = []
	for x in xrange(0, 255, 16):
		knot_hash.append(format(reduce(lambda x, y: x^y, the_list[x:x+16]), '02x'))
	return "".join(knot_hash)

total = 0
for index in range(6):
	the_hash =  calc_hash("-".join([data, str(index)]))
	bin_str = ""
	for x in the_hash:
		bin_str += "{0:04b}".format(int(x, 16))
	total += bin_str.count("1")
print "Total: %s" % total