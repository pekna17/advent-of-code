starta = 679
startb = 771
factora = 16807
factorb = 48271

def generator(start, factor, criteria=1):
	while True:
		start *= factor
		start %= 2147483647
		if start % criteria == 0:
			yield start & 0xffff

a = generator(starta, factora)
b = generator(startb, factorb)
count = sum(a.next() == b.next() for _ in range(40000000))
print "Part 1: %s" % count

a = generator(starta, factora, 4)
b = generator(startb, factorb, 8)
count = sum(a.next() == b.next() for _ in range(5000000))
print "Part 2: %s" % count