banks = []
with open("06-1.txt") as f:
	banks = [int(x.strip()) for x in f.readline().split()]
states = []
count = 0

while banks not in states:
	states.append(banks[:])
	count += 1
	index = banks.index(max(banks))
	blocks = banks[index]
	banks[index] = 0
	while blocks > 0:
		index = index + 1 if index < (len(banks)-1) else 0
		banks[index] += 1
		blocks -= 1
	
print "Count: %s" % count