steps = []
with open("11-1.txt") as f:
	steps = [x.strip() for x in f.readline().split(",")]

def calc_distance(x, y):
	steps_diag = abs(x)/0.5
	stops_vert = abs(y) - abs(x)
	return (steps_diag + stops_vert)

x = 0.0
y = 0.0
max_dist = 0
for step in steps:
	if step == "n":
		y -= 1
	elif step == "s":
		y += 1
	elif step == "nw":
		x -= 0.5
		y -= 0.5
	elif step == "ne":
		x += 0.5
		y -= 0.5
	elif step == "sw":
		x -= 0.5
		y += 0.5
	elif step == "se":
		x += 0.5
		y += 0.5
	max_dist = max(max_dist, calc_distance(x,y))
print "[%s,%s]" % (x, y)
print "Distance: %s" % calc_distance(x,y)
print "Max distance: %s" % max_dist