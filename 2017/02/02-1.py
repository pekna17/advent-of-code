rows = []
with open("02-1.txt") as f:
	rows = [x.strip().split() for x in f.readlines()]

print rows
checksum_parts = []
for r in rows:
	r = map(int, r)
	print "%s %s" % (max(r), min(r))
	checksum_parts.append(int(max(r)) - int(min(r)))
print sum(checksum_parts)
