rows = []
with open("02-1.txt") as f:
	rows = [x.strip().split() for x in f.readlines()]
checksum_parts = []
for r in rows:
	r = map(int, r)
	for index, num in enumerate(r):
		for x in r[index+1:]:
			if max(x, num) % min(x, num) == 0:
				checksum_parts.append(max(x, num) / min(x, num))
				break
print sum(checksum_parts)
