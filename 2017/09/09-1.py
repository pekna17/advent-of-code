data = ""
with open("09-1.txt") as f:
	data = f.readline().strip()

filtered_data = []
skip_next = False
is_garbage = False
garbage = 0
for index in range(len(data)):
	if skip_next:
		skip_next = False
		continue
	if data[index] == "!":
		skip_next = True
		continue
	if is_garbage:
		if data[index] == '>':
			is_garbage = False
			continue
		else:
			garbage += 1
			continue
	elif data[index] == "<":
		is_garbage = True
		continue
	filtered_data.append(data[index])

depth = 1
score = 0
for item in filtered_data:
	if item == "{":
		score += depth
		depth += 1
	if item == "}":
		depth -= 1
print "Score: %s" % score
print "Garbage: %s" % garbage