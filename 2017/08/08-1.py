import operator
import re
rows = []
used_rows = []
with open("08-1.txt") as f:
	rows = [x.strip() for x in f.readlines()]

values = {}
for row in rows:
	print row
	res = re.match("(.*) (.*) (.*) if (([a-z]+) .*)$", row)
	column = res.group(1)
	sign = -1 if res.group(2) == "dec" else 1
	value = res.group(3)
	condition = res.group(4)
	variable = res.group(5)
	variable_value = values[variable] if variable in values.keys() else 0
	condition_mod = condition.replace(variable, str(variable_value))
	print "Condition '%s' modified to '%s'" % (condition, condition_mod)
	if eval(condition_mod):
		column_value = values[column] if column in values.keys() else 0
		values[column] = int(column_value) + (sign * int(value))
print values
print "Max: %s" % max(values.iteritems(), key=operator.itemgetter(1))[1]