steps = []
with open("05-1.txt") as f:
	steps = [int(x.strip()) for x in f.readlines()]

index = 0
count = 0

while index < len(steps):
	step = steps[index]
	steps[index] += 1
	index += step
	count += 1
	#print steps
print "Count: %s" % count