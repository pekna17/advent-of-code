import re
with open("20-1.txt") as f:
	rows = [x.strip("\n") for x in f.readlines()]

points = []
velocity = []
acceleration = []

for row in rows:
	p, v, a  = row.split(", ")
	res = re.match(".*<[ ]?([-]?\d+),([-]?\d+),([-]?\d+)", p)
	points.append([int(x) for x in list(res.groups())])
	res = re.match(".*<[ ]?([-]?\d+),([-]?\d+),([-]?\d+)", v)
	velocity.append([int(x) for x in list(res.groups())])
	res = re.match(".*<[ ]?([-]?\d+),([-]?\d+),([-]?\d+)", a)
	acceleration.append([int(x) for x in list(res.groups())])

distance = [0 for x in range(len(points))]
last_distance_index = -1
distance_counter = 0
while distance_counter < 1000:
	for x in range(len(points)):
		for y in range(3):
			velocity[x][y]  += acceleration[x][y]
			points[x][y]  += velocity[x][y]
		distance[x] = abs(points[x][0]) + abs(points[x][1]) + abs(points[x][2])
	min_distance = min(distance)
	if distance.index(min_distance) == last_distance_index and distance.count(min_distance) == 1:
		distance_counter += 1
	else:
		last_distance_index = distance.index(min_distance)
		distance_counter = 0
print last_distance_index