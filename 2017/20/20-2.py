import re
import collections
with open("20-1.txt") as f:
	rows = [x.strip("\n") for x in f.readlines()]

points = []
velocity = []
acceleration = []

for row in rows:
	p, v, a  = row.split(", ")
	res = re.match(".*<[ ]?([-]?\d+),([-]?\d+),([-]?\d+)", p)
	points.append([int(x) for x in list(res.groups())])
	res = re.match(".*<[ ]?([-]?\d+),([-]?\d+),([-]?\d+)", v)
	velocity.append([int(x) for x in list(res.groups())])
	res = re.match(".*<[ ]?([-]?\d+),([-]?\d+),([-]?\d+)", a)
	acceleration.append([int(x) for x in list(res.groups())])

counter_since_remove = 0
while counter_since_remove < 10:
	# step
	for x in range(len(points)):
		for y in range(3):
			velocity[x][y]  += acceleration[x][y]
			points[x][y]  += velocity[x][y]
	# check for duplicates
	remove = []
	for index in range(len(points)):
		if points[index] in points[index+1:] and points[index] not in remove:
			remove.append(points[index])
	if len(remove):
		for r in remove:
			while points.count(r):
				index = points.index(r)
				del points[index]
				del acceleration[index]
				del velocity[index]
		counter_since_remove = 0
	else:
		counter_since_remove += 1
print len(points)