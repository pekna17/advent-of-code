the_list = list(range(256))
with open("10-1.txt") as f:
	steps = [ord(x) for x in list(f.readline().strip())] + [17, 31, 73, 47, 23]

skip_size = 0
position = 0
for the_round in range(64):
	for step in steps:
		list_to_reverse = []
		for x in range(step):
			index = (position + x) % 256
			list_to_reverse.append(the_list[index])
		list_to_reverse.reverse()
		
		for x in range(step):
			index = (position + x) % 256
			the_list[index] = list_to_reverse[x]

		position = (position + step + skip_size) % 256
		skip_size += 1

knot_hash = []
for x in xrange(0, 255, 16):
	print "x: %s, x+16: %s" % (x, x+16)
	knot_hash.append(format(reduce(lambda x, y: x^y, the_list[x:x+16]), '02x'))
print "Knot hash: %s" % "".join(knot_hash)