the_list = list(range(256))
steps = []
with open("10-1.txt") as f:
	steps = [int(x.strip()) for x in f.readline().split(",")]

skip_size = 0
position = 0
for step in steps:
	list_to_reverse = []
	for x in range(step):
		index = (position + x) % 256
		list_to_reverse.append(the_list[index])
	list_to_reverse.reverse()
	
	for x in range(step):
		index = (position + x) % 256
		the_list[index] = list_to_reverse[x]

	position = (position + step + skip_size) % 256
	skip_size += 1
print "Result: %s" % (the_list[0] * the_list[1])