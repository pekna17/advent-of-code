with open("19-1.txt") as f:
	rows = [x.strip("\n") for x in f.readlines()]
result = ""
y = 0
x = rows[0].index("|")
direction = "south"
steps = 0
while True:
	if y >= len(rows) or y < 0 or x >= len(rows[y]) or x < 0 or rows[y][x] == " ":
		break
	if rows[y][x] not in ["|", "-", "+"]:
		result += rows[y][x]
	if direction in ["south", "north"]:
		if rows[y][x] == "+":
			if x+1 < len(rows[y]) and rows[y][x+1] != " ":
				direction = "east"
				x += 1
			else:
				direction = "west"
				x -= 1
		else:
			y += 1 if direction == "south" else -1
	elif direction in ["east", "west"]:
		if rows[y][x] == "+":
			if y+1 < len(rows) and rows[y+1][x] != " ":
				direction = "south"
				y += 1
			else:
				direction = "north"
				y -= 1
		else:
			x += 1 if direction == "east" else -1
	steps += 1
print "Result: %s" % result
print "Steps: %s" % steps