from collections import defaultdict
in_grid = []
with open("22-1.txt") as f:
	in_grid = [x.strip("\n") for x in f.readlines()]

grid = defaultdict(int)
for y, value in enumerate(in_grid):
	for x in range(len(value)):
		grid[(x, y)] = (value[x] == "#") * 2 # CLEAN or INFECTED

x = (len(in_grid[0]) - 1) / 2
y = (len(in_grid) - 1) / 2
directions = [(0, -1), (1, 0), (0, 1), (-1, 0)] # UP, RIGHT, DOWN, LEFT
d = 0
count = 0
states = ['CLEAN', 'WEAKENED', 'INFECTED', 'FLAGGED']

for _ in range(10000000):
	if grid[(x, y)] == states.index('CLEAN'):
		# Turn left if clean
		d = d - 1 if d > 0 else 3
		grid[(x, y)] = states.index('WEAKENED')
	elif grid[(x, y)] == states.index('WEAKENED'):
		grid[(x, y)] = states.index('INFECTED')
		count += 1
	elif grid[(x, y)] == states.index('INFECTED'):
		# Turn right if infected
		d = (d + 1) % len(directions)
		grid[(x, y)] = states.index('FLAGGED')
	elif grid[(x, y)] == states.index('FLAGGED'):
		# Reverse if flagged
		d = (d + 2) % len(directions)
		grid[(x, y)] = states.index('CLEAN')
	# Move in current direction
	x = x + directions[d][0]
	y = y + directions[d][1]

print count