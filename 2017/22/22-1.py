from collections import defaultdict
in_grid = []
with open("22-1.txt") as f:
	in_grid = [x.strip("\n") for x in f.readlines()]

grid = defaultdict(int)
for y, value in enumerate(in_grid):
	for x in range(len(value)):
		grid[(x, y)] = value[x]

x = (len(in_grid[0]) - 1) / 2
y = (len(in_grid) - 1) / 2
directions = [(0, -1), (1, 0), (0, 1), (-1, 0)] # UP, RIGHT, DOWN, LEFT
d = 0
count = 0

for _ in range(10000):
	if grid[(x, y)] == "#":
		# Turn right if infected
		d = (d + 1) % len(directions)
		grid[(x, y)] = "."
	else:
		# Turn left
		d = d - 1 if d > 0 else 3
		grid[(x, y)] = "#"
		count += 1
	# Move in current direction
	x = x + directions[d][0]
	y = y + directions[d][1]
print count