import json
import re
rows = []
with open("07-1.txt") as f:
	rows = [x.strip() for x in f.readlines()]

holder = {}
prev_holder = {'Test': None}
while json.dumps(prev_holder) != json.dumps(holder):
	prev_holder = holder.copy()
	for row in rows:
		res = re.match("(.*) \(\d+\) \-\> (.*)$", row)
		if res:
			disks = res.group(2).split(", ")
			if not len(holder):
				holder[res.group(1)] = {d:0 for d in disks}
			else:
				for key, value in holder.iteritems():
					if key in disks:
						#we have a new parent
						new_holder = {}
						new_disks = {d:0 for d in disks}
						new_disks[key] = holder[key].copy()
						new_holder[res.group(1)] = new_disks
						holder = new_holder.copy()
						break
	print holder

print "Top disk: %s" % next(iter(holder))