import json
import pprint
import re
rows = []
used_rows = []
with open("07-1.txt") as f:
	rows = [x.strip() for x in f.readlines()]

pp = pprint.PrettyPrinter(indent=2)

def insert_child(holder, newkey, newdisks, newweight):
	for key, value in holder.iteritems():
		if key == newkey:
			holder[key]['disks'] = {d:{'disks': {}, 'weight': 0} for d in disks}
			holder[key]['weight'] = newweight
			return True
		if insert_child(holder[key]['disks'], newkey, newdisks, newweight):
			return True
	return False

def insert_weight(holder, newkey, newweight):
	for key, value in holder.iteritems():
		if key == newkey:
			holder[key]['weight'] = newweight
			return True
		if insert_weight(holder[key]['disks'], newkey, newweight):
			return True
	return False

holder = {}
while len(rows) > 0:
	for row in rows:
		res = re.match("(.*) \((\d+)\) \-\> (.*)$", row)
		if res:
			added = False
			disk = res.group(1)
			disks = res.group(3).split(", ")
			weight = int(res.group(2))
			if not len(holder):
				holder[disk] = {'disks': {d:{'disks': {}, 'weight': 0} for d in disks}, 'weight': weight}
				added = True
			else:
				# Check to insert new parent
				for key, value in holder.iteritems():
					if key in disks:
						#we have a new parent
						new_holder = {}
						new_holder[disk] = {'disks': {d:{'disks': {}, 'weight': 0} for d in disks}, 'weight': weight}
						new_holder[disk]['disks'][key] = {'disks': holder[key]['disks'].copy(), 'weight': holder[key]['weight']}
						holder = new_holder.copy()
						added = True
				if not added:
					added = insert_child(holder, disk, disks, weight)
		else:
			res = re.match("(.*) \((\d+)\)$", row)
			disk = res.group(1)
			weight = int(res.group(2))
			added = insert_weight(holder, disk, weight)

		if added:
			used_rows.append(row)
	rows = [x for x in rows if x not in used_rows]

top = next(iter(holder))
print "Top disk: %s" % top
#pp.pprint(holder)

def find_mismatch(holder):
	total_sum = holder['weight']
	keys = []
	sums = []
	weights = []
	for key, value in holder['disks'].iteritems():
		the_sum = find_mismatch(holder['disks'][key])
		total_sum += the_sum
		keys.append(key)
		sums.append(the_sum)
		weights.append(holder['disks'][key]['weight'])
	if len([x for x in sums if x != sums[-1]]):
		# First print here will show the error
		print "We have a mismatch, keys: %s, sums: %s, weights: %s" % (keys, sums, weights)
	return total_sum

find_mismatch(holder[top])