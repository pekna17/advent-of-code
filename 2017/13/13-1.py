rows = []
with open("13-1.txt") as f:
	rows = [x.strip() for x in f.readlines()]

data = {}
for row in rows:
	index, size = row.split(": ")
	data[int(index)] = int(size)

severity = 0
for key, value in data.iteritems():
	position = key % (2 * (value - 1))
	if position == 0:
		severity += key * value
print "Severity: %s" % severity
