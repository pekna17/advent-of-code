import operator
rows = []
with open("13-1.txt") as f:
	rows = [x.strip() for x in f.readlines()]

data = {}
for row in rows:
	index, size = row.split(": ")
	data[int(index)] = int(size)

found = False
delay = 0
sorteddata = sorted(data, key=data.__getitem__)
while not found:
	delay += 1
	found = True
	for key in sorteddata:
		value = data[key]
		position = (key + delay) % (2 * (value - 1))
		if position == 0:
			found = False
			break
print "Delay: %s" % delay