import sys
from math import sqrt, ceil
number = 289326

w, h = 15, 15;
matrix = [[0 for x in range(w)] for y in range(h)]

def print_matrix(matrix):
	for row in matrix:
		values = [str(x).rjust(6) for x in row]
		print values

def calculate_value(matrix, y, x):
	print "y:%s x:%s" % (y, x)
	return sum([matrix[y][x+1],
				matrix[y-1][x+1],
				matrix[y-1][x],
				matrix[y-1][x-1],
				matrix[y][x-1],
				matrix[y+1][x-1],
				matrix[y+1][x],
				matrix[y+1][x+1]])

def caluclate_next_step(matrix, y, x):
	if matrix[y][x+1] == matrix[y][x-1] == matrix[y+1][x] == matrix[y-1][x] == 0:
		print "All empty, move right"
		return y, x+1
	elif matrix[y][x-1] > 0 and matrix[y-1][x] == 0:
		print "Left set, up is free, move up"
		return y-1, x
	elif matrix[y+1][x] > 0 and matrix[y][x-1] == 0:
		print "below is set, left is empty, move left"
		return y, x-1
	elif matrix[y][x+1] == 0:
		print "right is empty, move right"
		return y, x+1
	elif matrix[y+1][x] == 0:
		print "below is empty, move down"
		return y+1, x
	print "ERROR"
	print "y:%s x:%s" % (y, x)
	print_matrix(matrix)
	sys.exit(1)

y, x = 7, 7
matrix[y][x] = 1
value = 0
while value < number:
	y, x = caluclate_next_step(matrix, y, x)
	value = calculate_value(matrix, y, x)
	matrix[y][x] = value

print_matrix(matrix)
print "Value=%s" % value