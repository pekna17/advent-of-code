from math import sqrt, ceil
number = 289326
root = sqrt(number)
print root
top = int(ceil(root))
if top % 2 == 0:
	top += 1
print top

right_bottom = top*top
left_bottom = right_bottom - top + 1
left_top = left_bottom - top + 1
right_top = left_top - top + 1

print right_bottom
print left_bottom
print left_top
print right_top

one_index = top / 2
print "one_index: %s" % one_index

def find_steps(number, one_index, lower, upper):
	print "Testing %s towards %s and %s" % (number, lower, upper)
	if number >= lower and number <= upper:
		index = number - lower
		print "index: %s" % index
		distance = abs(index - one_index)
		print "Steps for %s: %s" % (number, one_index + distance)
		return True
	return False

if not find_steps(number, one_index, left_bottom, right_bottom):
	if not find_steps(number, one_index, left_top, left_bottom):
		if not find_steps(number, one_index, right_top, left_top):
			find_steps(number, one_index, (right_top - top + 1), right_top)
