steps = []
with open("16-1.txt") as f:
	steps = [x for x in f.readline().strip().split(",")]

num_prog = 16
programs = [chr(x) for x in xrange(ord('a'), ord('a') + num_prog, 1)]

def dance(programs, reps):
	seen = []
	for _ in range(reps):
		seen.append("".join(programs))
		for step in steps:
			if step[0] == "s":
				pos = len(programs)-int(step[1:])
				programs = programs[pos:] + programs[0:pos]
			else:
				part1, part2 = step[1:].split("/")
				if step[0] == "x":
					part1, part2 = int(part1), int(part2)
					programs[part1], programs[part2] = programs[part2], programs[part1]
				if step[0] == "p":
					index1, index2 = programs.index(part1), programs.index(part2)
					programs[index1], programs[index2] = programs[index2], programs[index1]
		if "".join(programs) in seen:
			return seen[1000000000 % len(seen)]
	return "".join(programs)

print "Part1: %s" % dance(programs[:], 1)
print "Part2: %s" % dance(programs[:], 1000)
