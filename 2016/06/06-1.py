messages = []
with open("06-1.txt") as f:
	messages = [m.strip() for m in f.readlines()]

password = ""
for x in xrange(len(messages[0])):
	letters = [m[x] for m in messages]
	password += max(set(letters), key=letters.count)

print "Password: %s" % password