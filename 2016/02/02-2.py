matrix = [["0", "0", "1", "0", "0"],
		  ["0", "2", "3", "4", "0"],
		  ["5", "6", "7", "8", "9"],
		  ["0", "A", "B", "C", "0"],
		  ["0", "0", "D", "0", "0"]]
y = 2
x = 0

instructions = []
with open("02-1.txt") as f:
	instructions = [i.strip() for i in f.readlines()]

code = ""
for instruction in instructions:
	for step in instruction:
		if step == "U" and y > 0 and matrix[y-1][x] != "0":
			y -= 1
		elif step == "D" and y < 4 and matrix[y+1][x] != "0":
			y += 1
		elif step == "R" and x < 4 and matrix[y][x+1] != "0":
			x += 1
		elif step == "L" and x > 0 and matrix[y][x-1] != "0":
			x -= 1
	code += str(matrix[y][x])
print "Code: %s" % code