matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
y = 1
x = 1

instructions = []
with open("02-1.txt") as f:
	instructions = [i.strip() for i in f.readlines()]

code = ""
for instruction in instructions:
	for step in instruction:
		if step == "U" and y > 0:
			y -= 1
		elif step == "D" and y < 2:
			y += 1
		elif step == "R" and x < 2:
			x += 1
		elif step == "L" and x > 0:
			x -= 1
	code += str(matrix[y][x])
print "Code: %s" % code