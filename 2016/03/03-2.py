triangles = []
with open("03-1.txt") as f:
	triangles = [map(int, i.strip().split()) for i in f.readlines()]

valid = 0
for i in xrange(0, len(triangles), 3):
	for column in xrange(3):
		triangle = sorted([triangles[i][column], triangles[i+1][column], triangles[i+2][column]])
		if triangle[0] + triangle[1] > triangle[2]:
			valid += 1
print "Valid triangles: %s" % valid