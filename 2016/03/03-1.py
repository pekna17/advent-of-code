triangles = []
with open("03-1.txt") as f:
	triangles = [sorted(map(int, i.strip().split())) for i in f.readlines()]

valid = 0
for triangle in triangles:
	if triangle[0] + triangle[1] > triangle[2]:
		valid += 1
print "Valid triangles: %s" % valid