import hashlib
input = "ffykfhsq"

loop = 0
password = ""
while len(password) < 8:
	md5 = hashlib.md5()
	hash = input + str(loop)
	md5.update(hash)
	hex = md5.hexdigest()
	if hex.startswith("00000"):
		password += hex[5]
	loop += 1

print "Password: %s" % password