import hashlib
input = "ffykfhsq"

loop = 0
password = ["", "", "", "", "", "", "", ""]
while len("".join(password)) < 8:
	md5 = hashlib.md5()
	hash = input + str(loop)
	md5.update(hash)
	hex = md5.hexdigest()
	if hex.startswith("00000") and not hex[5].isalpha() and int(hex[5]) < 8 and password[int(hex[5])] == "":
		password[int(hex[5])] += hex[6]
	loop += 1

print "Password: %s" % "".join(password)