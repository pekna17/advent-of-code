import re

rooms = []
with open("04-1.txt") as f:
	rooms = [i.strip() for i in f.readlines()]

for room in rooms:
	parts = re.match("([a-z\-]*)-(\d+)\[(.*)\]", room)
	name = " ".join(parts.group(1).split("-"))
	sectorid = int(parts.group(2))
	
	for loop in xrange(sectorid):
		new_name = ""
		for index, x in enumerate(name):
			if x != " ":
				new_name += chr(ord(x)+1) if chr(ord(x)) < 'z' else 'a'
			else:
				new_name += x
		name = new_name
	if "northpole" in name:
		print "'%s' have sectorid: %s" % (name, sectorid)
		break