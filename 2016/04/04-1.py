import re
from collections import Counter
from operator import itemgetter

rooms = []
with open("04-1.txt") as f:
	rooms = [i.strip() for i in f.readlines()]

def sortme(x, y):
	if x[1] > y[1]:
		return -1
	elif x[1] < y[1]:
		return 1
	elif x[0] < y[0]:
		return -1
	elif x[0] > y[0]:
		return 1
	return 0

total_sectorid = 0
for room in rooms:
	parts = re.match("([a-z\-]*)-(\d+)\[(.*)\]", room)
	letters = "".join(parts.group(1).split("-"))
	sectorid = int(parts.group(2))
	checksum = parts.group(3)
	valid_letters = [x for x in letters if x in checksum]
	invalid_letters = [x for x in letters if not x in checksum]
	result = sorted(Counter(letters).most_common(), cmp=sortme)[0:len(checksum)]
	result = dict(result)
	# TODO: compare towards checksum
	if 0 == len([x for x in checksum if x not in result]):
		total_sectorid += sectorid

print "Sectorid: %s" % total_sectorid