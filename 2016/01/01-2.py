import sys
x = 300
y = 300
direction = "NORTH"

w, h = 600, 600;
matrix = [[0 for x1 in range(w)] for y1 in range(h)]
matrix[y][x] = 1

steps = []
with open("01-1.txt") as f:
	steps = [step.strip() for step in f.readline().split(",")]

def print_matrix(matrix):
	for row in matrix:
		values = [str(x).rjust(2) for x in row]
		print values

number = 1
def calculate_new_position(matrix, x, y, previous_direction, step, number):
	if previous_direction == "EAST":
		direction = "NORTH" if step[0] == "L" else "SOUTH"
	elif previous_direction == "NORTH":
		direction = "WEST" if step[0] == "L" else "EAST"
	elif previous_direction == "WEST":
		direction = "SOUTH" if step[0] == "L" else "NORTH"
	elif previous_direction == "SOUTH":
		direction = "EAST" if step[0] == "L" else "WEST"
	
	print step
	print direction
	if direction == "EAST":
		for index in range(int(step[1:])):
			x += 1
			if matrix[y][x] > 0:
				print "We have match %s,%s" % (x, y)
				return matrix, x, y, direction, True
			matrix[y][x] = number
	elif direction == "WEST":
		for index in range(int(step[1:])):
			x -= 1
			if matrix[y][x] > 0:
				print "We have match %s,%s" % (x, y)
				return matrix, x, y, direction, True
			matrix[y][x] = number
	elif direction == "SOUTH":
		for index in range(int(step[1:])):
			y += 1
			if matrix[y][x] > 0:
				print "We have match %s,%s" % (x, y)
				return matrix, x, y, direction, True
			matrix[y][x] = number
	elif direction == "NORTH":
		for index in range(int(step[1:])):
			y -= 1
			if matrix[y][x] > 0:
				print "We have match %s,%s" % (x, y)
				return matrix, x, y, direction, True
			matrix[y][x] = number
	else:
		print "ERROR"
		sys.exit(1)
	return matrix, x, y, direction, False

for step in steps:
	matrix, x, y, direction, found_match = calculate_new_position(matrix, x, y, direction, step, number)
	#print_matrix(matrix)
	number += 1
	if found_match:
		break
print "%s, %s" % (x, y)
print "Steps: %s" % (abs(x) + abs(y) - 600)
