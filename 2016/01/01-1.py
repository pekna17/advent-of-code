x = 0
y = 0
direction = "NORTH"

w, h = 300, 300;
matrix = [[0 for x1 in range(w)] for y1 in range(h)]


steps = []
with open("01-1.txt") as f:
	steps = [step.strip() for step in f.readline().split(",")]


def calculate_new_position(x, y, previous_direction, step):
	if previous_direction == "EAST":
		direction = "NORTH" if step[0] == "L" else "SOUTH"
	elif previous_direction == "NORTH":
		direction = "WEST" if step[0] == "L" else "EAST"
	elif previous_direction == "WEST":
		direction = "SOUTH" if step[0] == "L" else "NORTH"
	elif previous_direction == "SOUTH":
		direction = "EAST" if step[0] == "L" else "WEST"
	
	if direction == "EAST":
		x += int(step[1:])
	elif direction == "WEST":
		x -= int(step[1:])
	elif direction == "SOUTH":
		y += int(step[1:])
	elif direction == "NORTH":
		y -= int(step[1:])
	return x, y, direction

for step in steps:
	x, y, direction = calculate_new_position(x, y, direction, step)
print "%s, %s" % (x, y)
print "Steps: %s" % (abs(x) + abs(y))