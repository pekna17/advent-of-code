import string 
polymer = ""
with open("input.txt") as f:
	polymer = f.readline().strip()

letters = list(string.ascii_lowercase)
old_polymer = ""
while polymer != old_polymer:
	old_polymer = polymer
	for letter in letters:
		polymer = polymer.replace(letter+letter.upper(), "")
		polymer = polymer.replace(letter.upper()+letter, "")
	
print(len(polymer))