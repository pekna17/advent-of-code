from collections import defaultdict

metadata = []
with open("input.txt") as f:
	metadata = [list(map(int, f.strip().split(" "))) for f in f.readlines()]


metadata = metadata[0]
def get_node(index):
	#print("enter with index: %s" % index)
	summa = 0
	child_data = defaultdict(int)
	childs = metadata[index]
	meta_entries = metadata[index+1]
	#print("Childs: %s, metas: %s" % (childs, meta_entries))
	for child in range(0, childs):
		#print(index, child)
		index, summa2 = get_node(index+2)
		#print(summa2)
		child_data[child+1] = summa2

	if childs == 0:
		for meta in range(index+2, index+2+meta_entries):
			summa += metadata[meta]
	else:
		for meta in range(index+2, index+2+meta_entries):
			summa += child_data[metadata[meta]]

	index += meta_entries
	#print("Summa: %s" % summa)
	return index, summa




index = 0
_, summa = get_node(0)


print(summa)