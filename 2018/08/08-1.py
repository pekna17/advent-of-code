import operator

metadata = []
with open("input.txt") as f:
	metadata = [list(map(int, f.strip().split(" "))) for f in f.readlines()]


metadata = metadata[0]
def get_node(index):
	#print("enter with index: %s" % index)
	summa = 0
	childs = metadata[index]
	meta_entries = metadata[index+1]
	#print("Childs: %s, metas: %s" % (childs, meta_entries))
	for child in range(0, childs):
		#print(index, child)
		index, summa2 = get_node(index+2)
		summa += summa2

	for meta in range(index+2, index+2+meta_entries):
		summa += metadata[meta]
	index += meta_entries
	#print("Summa: %s" % summa)
	return index, summa




index = 0
index, summa = get_node(0)


print(summa)