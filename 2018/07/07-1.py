from collections import defaultdict

indata = []
with open("input.txt") as f:
	indata = [(f.strip().split(" ")[1], f.strip().split(" ")[7]) for f in f.readlines()]

letters = set()
for (before, after) in indata:
	letters.add(before)
	letters.add(after)
letters = sorted(letters)

print(letters)
