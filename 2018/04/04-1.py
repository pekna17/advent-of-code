#import date
import re 
import datetime
from collections import defaultdict
import operator

rows = []
with open("input.txt") as f:
	rows = [x.strip()[1:].split("] ") for x in f.readlines()]

def get_key(item):
	return item[0]
rows = sorted(rows, key=get_key)

def get_date(date_str):
	date_parts = re.match("(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d)", date_str)
	year = int(date_parts.group(1))+500
	month = date_parts.group(2)
	day = date_parts.group(3)
	hour = int(date_parts.group(4))
	minute = int(date_parts.group(5))

	base_date = datetime.date.fromisoformat("%s-%s-%s" % (year, month, day))
	if hour == 23:
		hour = 0
		minute = 0
		base_date += datetime.timedelta(days=1)

	return base_date.year, base_date.month, base_date.day, hour, minute

Matrix = {(x,y):0 for x in range(1100) for y in range(1100)}
guard_total_minutes = defaultdict(int)
guards_minutes = {}
for row in rows:
	year, month, day, hour, minute = get_date(row[0])
	print(row)
	if "#" in row[1]:
		guard = int(re.match(".*#(.*) b.*", row[1]).group(1))
		print(guard)
	elif "falls" in row[1]:
		start = minute
		print(start)
	elif "wakes" in row[1]:
		stop = minute
		print(stop)
		guard_total_minutes[guard] += stop-start
		print(guard_total_minutes[guard])
		for minn in range(start, stop+1):
			if guard not in guards_minutes:
				guards_minutes[guard] = defaultdict(int)
			guards_minutes[guard][minn] += 1

for guard, minu in guard_total_minutes.items():
	print(guard, "-", minu)

# Guard with max minutes sleep
guard = max(guard_total_minutes.items(), key=operator.itemgetter(1))[0]

# Times sleept on most common minute
times = max(guards_minutes[guard].items(), key=operator.itemgetter(1))[0]
print("%s*%s=%s" %(guard, times, (guard*times)))
print(guards_minutes[guard])


maxa_per_guard = defaultdict()
for guard, mindata in guards_minutes.items():
	maxa_per_guard[guard] = max(mindata.items(), key=operator.itemgetter(1))[0]

print(maxa_per_guard)
svar = max(maxa_per_guard.items(), key=operator.itemgetter(1))

print(svar)