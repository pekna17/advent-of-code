rows = []
with open("input.txt") as f:
	rows = [int(x.strip()) for x in f.readlines()]

print(sum(rows))