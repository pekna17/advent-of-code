rows = []
with open("input.txt") as f:
	rows = [int(x.strip()) for x in f.readlines()]

the_sums = []
last_sum = 0
while True:
	for r in rows:
		the_sums.append(last_sum)
		last_sum += r
		if last_sum in the_sums:
			print(last_sum)
			exit(0)
