rows = []
with open("input.txt") as f:
	rows = ["".join(x.strip().split("@ ")[1:]).split(": ") for x in f.readlines()]

#print(rows)

Matrix = {(x,y):0 for x in range(1100) for y in range(1100)}
for r in rows:
	x,y = r[0].split(",")
	x2,y2 = r[1].split("x")

	#print(x, y, x2, y2)
	overlapped = False
	for xpos in range(int(x)+1, int(x)+int(x2)+1):
		for ypos in range(int(y)+1, int(y)+int(y2)+1):
			Matrix[(xpos, ypos)] = Matrix[(xpos, ypos)] + 1

index = 0
for r in rows:
	index += 1
	x,y = r[0].split(",")
	x2,y2 = r[1].split("x")

	overlapped = False
	for xpos in range(int(x)+1, int(x)+int(x2)+1):
		for ypos in range(int(y)+1, int(y)+int(y2)+1):
			if Matrix[(xpos, ypos)] > 1:
				overlapped = True
	if not overlapped:
		print(index)
		break

