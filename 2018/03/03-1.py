rows = []
with open("input.txt") as f:
	rows = ["".join(x.strip().split("@ ")[1:]).split(": ") for x in f.readlines()]

#print(rows)

Matrix = {(x,y):0 for x in range(1100) for y in range(1100)}
counter = 0
for r in rows:
	x,y = r[0].split(",")
	x2,y2 = r[1].split("x")

	#print(x, y, x2, y2)

	for xpos in range(int(x)+1, int(x)+int(x2)+1):
		for ypos in range(int(y)+1, int(y)+int(y2)+1):
			if Matrix[(xpos, ypos)] < 2:
				Matrix[(xpos, ypos)] = Matrix[(xpos, ypos)] + 1
				#print(xpos, ypos)
				if Matrix[(xpos, ypos)] > 1:
					counter += 1
#print(Matrix)
print(counter)

#for x in range(10):
#	for y in range(10):

