rows = []
with open("input.txt") as f:
	rows = [list(x.strip()) for x in f.readlines()]

for index in range(0, 5):
	lines = [r[:index] + r[index+1:] for r in rows]

	for l in lines:
		if lines.count(l) == 2:
			print("".join(l))
			exit(0) 
