rows = []
with open("input.txt") as f:
	rows = [list(x.strip()) for x in f.readlines()]

twos = 0
threes = 0
for r in rows:
	two = False
	three = False
	for l in r:
		c = r.count(l)
		if c == 2:
			two = True 
		if c == 3:
			three = True
	if two:
		twos += 1 
	if three:
		threes += 1 
print(twos * threes)