import operator

coords = []
with open("input.txt") as f:
	coords = [list(map(int, f.strip().split(", "))) for f in f.readlines()]

maxx = max(coords, key=operator.itemgetter(0))[0]
minx = min(coords, key=operator.itemgetter(0))[0]
maxy = max(coords, key=operator.itemgetter(1))[1]
miny = min(coords, key=operator.itemgetter(1))[1]

for x in range(minx, maxx+1):
	for y in range(miny, maxy+1):
		

print(maxx, minx, maxy, miny)